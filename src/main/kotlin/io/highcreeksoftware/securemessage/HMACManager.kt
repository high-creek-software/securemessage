package io.highcreeksoftware.securemessage

import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

interface HMACManager {
    fun hashMessage(message: String): String
    fun verifyMessage(hash: String): String
}

class HMACManagerImpl(secretKey: String, private val algo: String = "HmacSHA256"): HMACManager {

    private val pipe = "|".toByteArray()[0]
    private val keySec = SecretKeySpec(secretKey.toByteArray(), algo)

    override fun hashMessage(message: String): String {
        // Do HMAC of the message
        val mac = Mac.getInstance(algo)
        mac.init(keySec)
        val macResult = mac.doFinal(message.toByteArray())
        // Then set value to: message|hmac
        val bs = message.toByteArray() + pipe + macResult
        // Base64 encode the value.
        return Base64.getEncoder().encodeToString(bs)
    }

    override fun verifyMessage(hash: String): String {
        // Get bytes of hash and message
        val bs = Base64.getDecoder().decode(hash)
        val index = bs.indexOf(pipe)
        val message = bs.slice(IntRange(0, index - 1)).toByteArray()

        // Check the hmac of the incoming hash
        val mac = Mac.getInstance(algo)
        mac.init(keySec)
        val macResult = mac.doFinal(message)
        if(macResult.contentEquals(bs.slice(IntRange(index + 1, bs.size - 1)).toByteArray())) {
            return String(message)
        }

        throw Exception("hmacs do not match")
    }

}