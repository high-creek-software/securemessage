package io.highcreeksoftware.securemessage

import org.junit.Test
import kotlin.test.assertEquals

class HMACManagerTest {

    @Test
    fun testHashMessage() {
        val hmacManager = HMACManagerImpl("not-secret-key")
        val message = "this-is-a-message"

        val hash = hmacManager.hashMessage(message)
        println("Hash: $hash")

        val result = hmacManager.verifyMessage(hash)
        assertEquals(message, result)
    }
}